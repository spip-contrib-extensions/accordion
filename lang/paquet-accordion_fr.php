<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accordion_description' => 'Utilisez accordion de JQueryUI dans votre contenu avec le raccourci accordeon et les intertitres SPIP. Pour SPIP4 et supérieur, vous devez installer le plugin jquery_ui https://git.spip.net/spip/jquery_ui.git',
	'accordion_nom' => 'Raccourci Accordéon',
	'accordion_slogan' => 'Des paragraphes repliables en accordéon',
);